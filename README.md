# Pre-built-PH-plugins-extra.
Extra plugins for [Process Hacker](https://github.com/processhacker/processhacker), here's the [source](https://github.com/processhacker/plugins-extra).
Currently built using the source as of commit a1175bf, you can always build it yourself if you don't trust this one, I'm just sharing them for convinience ;) .
Hope someone has a use for this.

Two plugins are missing, currently MemoryExt and ReparseEnum, as they gave me lots of errors within Visual Studio and I'm no C developer.

Cheers~
